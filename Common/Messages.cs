﻿

namespace Common
{
    public static class Messages
    {
        public const string RequiredAuctionId = "Auction Id is required";
        public const string InvalidInputValues = "Invalid input values.";
        public const string InvalidCustomer = "Customer not found.";
        public const string InvalidAuction = "Auction not found.";
    }
}