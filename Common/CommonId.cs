﻿using System;

namespace Common
{
    public class CommonId
    {
        public static string NewId { get { return Guid.NewGuid().ToString("N"); } }
    }
}
