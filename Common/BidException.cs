﻿using System;

namespace Common
{
    public class BidException:Exception
    {
        public BidException(string message):base(message)
        {
        }
    }
}
