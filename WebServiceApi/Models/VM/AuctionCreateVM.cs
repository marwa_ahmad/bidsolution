﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebServiceApi.Models.VM
{
    public class AuctionCreateVM
    {        
        [Required]
        public DateTime StartDate { get; set; }
        
        [Required]
        public List<ItemAuctionVM> Items { get; set; }
    }
}