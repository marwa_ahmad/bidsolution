﻿
using System.ComponentModel.DataAnnotations;

namespace WebServiceApi.Models.VM
{
    public class ItemCreateVm
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}