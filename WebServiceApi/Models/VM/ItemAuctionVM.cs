﻿using System.ComponentModel.DataAnnotations;

namespace WebServiceApi.Models.VM
{
    public class ItemAuctionVM
    {
        [Required]
        public string ItemId { get; set; }

        [Required]
        public float InitialPrice { get; set; }
    }
}