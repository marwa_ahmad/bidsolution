﻿
using System.ComponentModel.DataAnnotations;

namespace WebServiceApi.Models.VM
{
    public class BidCreateVM
    {
        [Required]
        public string CustomerId { get; set; }
        
        [Required]        
        public string ItemId { get; set; }

        [Required]        
        public double BidPrice { get; set; }

        [Required]        
        public string AuctionId { get; set; }
    }
}