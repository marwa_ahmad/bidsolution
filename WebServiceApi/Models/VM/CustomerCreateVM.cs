﻿
namespace WebServiceApi.Models.VM
{
    public class CustomerCreateVM
    {             
        //this value will be filled after an identity user is inserted into DB using aspnet identity; userId
        public string ReferenceUserId { get; set; }
    }
}