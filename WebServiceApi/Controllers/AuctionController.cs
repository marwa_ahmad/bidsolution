﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLayer;
using Common;
using WebServiceApi.Common;
using WebServiceApi.Models.VM;

namespace WebServiceApi.Controllers
{
    public class AuctionController : ApiController
    {
        // GET: Auction
        /// <summary>
        /// creates a new auction
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage Post([FromBody]AuctionCreateVM model)
        {
            if (model == null || ModelState.IsValid == false)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid input values.");
            }

            new AuctionBL().Create(model.ToModel());

            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        /// <summary>
        /// subscribe a customer to a certain auction
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public HttpResponseMessage Put(string id, [FromBody] string customerId)
        {
            if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(customerId))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.RequiredAuctionId);
            }
            if (ModelState.IsValid == false)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.InvalidInputValues);
            }

            new AuctionBL().SubscribeCustomer(id, customerId);

            return Request.CreateResponse(HttpStatusCode.Accepted);
        }
    }
}