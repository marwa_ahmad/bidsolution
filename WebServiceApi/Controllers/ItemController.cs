﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLayer;
using Common;
using WebServiceApi.Common;
using WebServiceApi.Models.VM;

namespace WebServiceApi.Controllers
{
    //[Authorize] //TODO to use Admin Role
    public class ItemController : ApiController
    {

        /// <summary>
        /// creates a new item
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage Post([FromBody]ItemCreateVm model)
        {
            if (model == null || ModelState.IsValid == false)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.InvalidInputValues);
            }

            new ItemBL().Create(model.ToModel());
            
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        //// GET api/values
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //public void Delete(int id)
        //{
        //}
    }
}
