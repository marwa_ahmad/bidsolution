﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLayer;
using Common;
using WebServiceApi.Common;
using WebServiceApi.Models.VM;

namespace WebServiceApi.Controllers
{
    public class BidController : ApiController
    {
        /// <summary>
        /// creates a new bid for a customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public HttpResponseMessage Post([FromBody]BidCreateVM viewModel)
        {
            if (viewModel == null || ModelState.IsValid == false)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.InvalidInputValues);
            }
            var bidBL = new BidBL();
            bidBL.Create(viewModel.ToModel());
            var highestAuctionItemBid = bidBL.GetHighestBid(viewModel.AuctionId, viewModel.ItemId);

            return Request.CreateResponse(HttpStatusCode.Accepted, highestAuctionItemBid.ToGetViewModel());
        }

        /// <summary>
        /// get the highest bid price
        /// </summary>
        /// <param name="auctionId"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public HttpResponseMessage Get(string auctionId, string itemId)
        {
            var highestAuctionItemBid = new BidBL().GetHighestBid(auctionId, itemId);
            return Request.CreateResponse(HttpStatusCode.OK, highestAuctionItemBid.ToGetViewModel());            
        }

    }
}
