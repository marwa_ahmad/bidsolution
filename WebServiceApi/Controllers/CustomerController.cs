﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessLayer;
using Common;
using WebServiceApi.Common;
using WebServiceApi.Models.VM;

namespace WebServiceApi.Controllers
{
    public class CustomerController : ApiController
    {
        /// <summary>
        /// creates a new customer
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public HttpResponseMessage Post([FromBody]CustomerCreateVM viewModel)
        {
            if (viewModel == null || ModelState.IsValid == false)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Messages.InvalidInputValues);
            }
            var customerBL = new CustomerBL();
            customerBL.Create(viewModel.ToModel());            

            return Request.CreateResponse(HttpStatusCode.Accepted);
        }

        /// <summary>
        /// lists all customers
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            var customerBL = new CustomerBL();
            var customers = customerBL.GetAll();    
            return Request.CreateResponse(HttpStatusCode.OK, customers.ToViewModel());            
        }
    }
}
