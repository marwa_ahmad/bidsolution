﻿using System.Collections.Generic;
using System.Linq;
using DAL;
using WebServiceApi.Models.VM;

namespace WebServiceApi.Common
{
    public static class Converter
    {
        public static Item ToModel(this ItemCreateVm viewModel)
        {
            if (viewModel == null) return null;
            return new Item()
            {
                Name = viewModel.Name,
                Description = viewModel.Description
            };
        }

        public static Auction ToModel(this AuctionCreateVM viewModel)
        {
            if (viewModel == null) return null;
            var itemAuctions = viewModel.Items.Select(itemAuction => new ItemAuction()
            {
                ItemId = itemAuction.ItemId, 
                InitialPrice = itemAuction.InitialPrice
            }).ToList();
            
            return new Auction()
            {
                StartDate = viewModel.StartDate,
                ItemAuction = itemAuctions                
            };
        }

        public static Bid ToModel(this BidCreateVM viewModel)
        {
            if (viewModel == null) return null;
            return new Bid()
            {
                AuctionId = viewModel.AuctionId,
                BidPrice = viewModel.BidPrice,
                CustomerId = viewModel.CustomerId,
                ItemId = viewModel.ItemId
            };
        }

        public static BidGetVM ToGetViewModel(this Bid model)
        {
            if (model == null) return null;
            return new BidGetVM()
            {
                Id = model.Id,
                BidPrice = model.BidPrice.Value,
                ItemId = model.ItemId,
                AuctionId = model.AuctionId,
                CustomerId = model.CustomerId
            };
        }

        public static Customer ToModel(this CustomerCreateVM viewModel)
        {
            return viewModel == null ? null : new Customer()
            {
                ReferenceUserId = viewModel.ReferenceUserId
            };
        }

        public static List<CustomerGetVM> ToViewModel(this List<Customer> modelCustomers)
        {
            if (modelCustomers == null || modelCustomers.Any() == false) return null;
            return modelCustomers.Select(customerModel => new CustomerGetVM
            {
                Id = customerModel.Id, 
                ReferenceUserId = customerModel.ReferenceUserId
            }).ToList();
        }
    }
}