﻿using System.Collections.Generic;

namespace BusinessLayer.Contracts
{
    public interface IMainBL<T, KeyType>
    {
        void Create(T entity);
        List<T> GetAll();
        void Update(T entity);
        T GetById(KeyType key);
    }
}
