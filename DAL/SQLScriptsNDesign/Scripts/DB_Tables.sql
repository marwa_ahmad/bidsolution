/*DB creation*/
USE master;
GO
	IF (NOT EXISTS (
					SELECT name 
					FROM master.dbo.sysdatabases 
					WHERE name = 'BidDB'))
		CREATE DATABASE BidDB
GO
/*Tables creation*/
USE BidDB
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Item]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Item](
		[Id] [varchar](32) NOT NULL,
		[Name] [nvarchar](500) NOT NULL,
		[Description] [nvarchar](max) NULL,
	 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END
GO

SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer](
	[Id] [varchar](32) NOT NULL,
	[Notes] [nvarchar](max) NULL,
	[ReferenceUserId] [nvarchar](50) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Auction]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[Auction](
		[Id] [varchar](32) NOT NULL,
		[CreationDate] [datetime] NULL,
		[StartDate] [datetime] NULL,
	 CONSTRAINT [PK_Auction] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
End
GO

SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerAuction]') AND type in (N'U'))
	BEGIN
	CREATE TABLE [dbo].[CustomerAuction](
		[Id] [varchar](32) NOT NULL,
		[CustomerId] [varchar](32) NULL,
		[AuctionId] [varchar](32) NULL,
	 CONSTRAINT [PK_CustomerAuction] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[CustomerAuction]  WITH CHECK ADD  CONSTRAINT [FK_CustomerAuction_Auction] FOREIGN KEY([AuctionId])
REFERENCES [dbo].[Auction] ([Id])

ALTER TABLE [dbo].[CustomerAuction] CHECK CONSTRAINT [FK_CustomerAuction_Auction]

ALTER TABLE [dbo].[CustomerAuction]  WITH CHECK ADD  CONSTRAINT [FK_CustomerAuction_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])

ALTER TABLE [dbo].[CustomerAuction] CHECK CONSTRAINT [FK_CustomerAuction_Customer]
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ItemAuction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ItemAuction](
	[Id] [varchar](32) NOT NULL,
	[ItemId] [varchar](32) NULL,
	[AuctionId] [varchar](32) NULL,
	[InitialPrice] [float] NULL,
 CONSTRAINT [PK_ItemAuction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[ItemAuction]  WITH CHECK ADD  CONSTRAINT [FK_ItemAuction_Auction] FOREIGN KEY([AuctionId])
REFERENCES [dbo].[Auction] ([Id])

ALTER TABLE [dbo].[ItemAuction] CHECK CONSTRAINT [FK_ItemAuction_Auction]

ALTER TABLE [dbo].[ItemAuction]  WITH CHECK ADD  CONSTRAINT [FK_ItemAuction_Item] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Item] ([Id])

ALTER TABLE [dbo].[ItemAuction] CHECK CONSTRAINT [FK_ItemAuction_Item]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF NOT EXISTS(SELECT object_id FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbBid_order]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Bid](
	[Id] [varchar](32) NOT NULL,
	[CustomerId] [varchar](32) NULL,
	[ItemId] [varchar](32) NULL,
	[BidPrice] [float] NULL,
	[CreationDate] [datetime] NULL,
	[AuctionId] [varchar](32) NULL,
 CONSTRAINT [PK_Bid] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[Bid]  WITH CHECK ADD  CONSTRAINT [FK_Bid_Auction] FOREIGN KEY([Id])
REFERENCES [dbo].[Auction] ([Id])


ALTER TABLE [dbo].[Bid] CHECK CONSTRAINT [FK_Bid_Auction]


ALTER TABLE [dbo].[Bid]  WITH CHECK ADD  CONSTRAINT [FK_Bid_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])

ALTER TABLE [dbo].[Bid] CHECK CONSTRAINT [FK_Bid_Customer]

ALTER TABLE [dbo].[Bid]  WITH CHECK ADD  CONSTRAINT [FK_Bid_Item] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Item] ([Id])

ALTER TABLE [dbo].[Bid] CHECK CONSTRAINT [FK_Bid_Item]
END
GO


