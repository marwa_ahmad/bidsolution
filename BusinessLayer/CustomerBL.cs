﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Contracts;
using Common;
using DAL;

namespace BusinessLayer
{
    public class CustomerBL : ICustomerBL<Customer>
    {
        public void Create(Customer entity)
        {
            entity.Id = CommonId.NewId;
            using (var context = new BidDBEntities())
            {
                context.Customer.Add(entity);
                context.SaveChanges();
            }
        }

        public List<Customer> GetAll()
        {
            using (var context = new BidDBEntities())
            {
                var customers = context.Customer.ToList();
                return customers;
            }
        }

        //not needed now
        public void Update(Customer entity)
        {
            throw new NotImplementedException();
        }

        public Customer GetById(string key)
        {
            using (var context = new BidDBEntities())
            {
                return context.Customer.FirstOrDefault(i => i.Id.Equals(key));
            }
        }
    }
}
