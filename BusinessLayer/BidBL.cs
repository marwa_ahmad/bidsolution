﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Contracts;
using Common;
using DAL;

namespace BusinessLayer
{
    public class BidBL:IBidBL<Bid>
    {
        public void Create(Bid entity)
        {
            entity.Id = CommonId.NewId;
            entity.CreationDate = DateTime.UtcNow;// in order to unify the datetime value
            using (var context = new BidDBEntities())
            {
                context.Bid.Add(entity);
                context.SaveChanges();
            }
        }

        //don't need it right now
        public List<Bid> GetAll()
        {
            throw new NotImplementedException();
        }
        //don't need it right now
        public void Update(Bid entity)
        {
            throw new NotImplementedException();
        }

        public Bid GetById(string key)
        {
            using (var context = new BidDBEntities())
            {
                return context.Bid.FirstOrDefault(i => i.Id.Equals(key));
            }
        }

        public Bid GetHighestBid(string auctionId, string itemdId)
        {
            using (var context = new BidDBEntities())
            {
                //Scenario 2: user bidding on an item
                var itemBids = context.Bid.Where(b => b.ItemId.Equals(itemdId) && b.AuctionId.Equals(auctionId)).ToList();
                if (itemBids == null || itemBids.Any() == false) return null;
                var itemBidsCustomers = itemBids.Select(ib => ib.CustomerId).Distinct().ToList();
                if (itemBidsCustomers.Count == 1) return itemBids.FirstOrDefault();

                //Scenario 3: multiple users bidding on an item - you are first; if n customers are bidding the auction; get the highest bid price
                //else
                if (itemBidsCustomers.Count == itemBids.Count)
                {
                    return itemBids.OrderByDescending(o => o.CreationDate).LastOrDefault();                    
                }

                // if multiple users continue to bid; i.e. not their first bid then get the highest bid
                return itemBids.OrderByDescending(o => o.BidPrice).FirstOrDefault();                    
            }
        }
    }
}
