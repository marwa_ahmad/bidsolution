﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Contracts;
using Common;
using DAL;

namespace BusinessLayer
{
    public class ItemBL :IItemBL <Item>
    {
        public void Create(Item entity)
        {
            entity.Id = CommonId.NewId;
            using (var context = new BidDBEntities())
            {                
                context.Item.Add(entity);
                context.SaveChanges();                
            }
        }

        public List<Item> GetAll()
        {
            using (var context = new BidDBEntities())
            {
                var items = context.Item.ToList();
                return items;
            }
        }

        public void Update(Item entity)
        {
            if(entity == null) throw new InvalidOperationException();
            Item item;            
            using (var context = new BidDBEntities())
            {
                item = context.Item.FirstOrDefault(i => i.Id.Equals(entity.Id));
            }

            if (item == null) return;

            item.Name = entity.Name;
            item.Description = entity.Description;

            using (var context = new BidDBEntities())
            {                
                context.Entry(item).State = System.Data.Entity.EntityState.Modified;               
                context.SaveChanges();
            }
        }

        public Item GetById(string key)
        {
            using (var context = new BidDBEntities())
            {
                return context.Item.FirstOrDefault(i => i.Id.Equals(key));
            }
        }
    }
}
