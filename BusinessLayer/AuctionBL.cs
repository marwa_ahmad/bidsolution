﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Contracts;
using Common;
using DAL;

namespace BusinessLayer
{
    public class AuctionBL : IAuctionBL<Auction>
    {
        public void Create(Auction entity)
        {
            if (entity == null) throw new BidException(Messages.InvalidInputValues);

            entity.Id = CommonId.NewId;
            entity.CreationDate = DateTime.UtcNow;
            foreach (var itemAuction in entity.ItemAuction)
            {
                itemAuction.AuctionId = entity.Id;
                itemAuction.Id = CommonId.NewId;
            }
            using (var context = new BidDBEntities())
            {
                context.Auction.Add(entity);
                context.SaveChanges();
            }
        }

        public List<Auction> GetAll()
        {
            using (var context = new BidDBEntities())
            {
                var auctions = context.Auction.ToList();
                return auctions;
            }
        }

        public void Update(Auction entity)
        {
            if (entity == null) throw new BidException(Messages.InvalidInputValues);
            Auction auction;
            using (var context = new BidDBEntities())
            {
                auction = context.Auction.FirstOrDefault(i => i.Id.Equals(entity.Id));
            }

            if (auction == null) return;

            auction = entity;

            using (var context = new BidDBEntities())
            {
                context.Entry(auction).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public Auction GetById(string key)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new BidException(Messages.InvalidInputValues);

            using (var context = new BidDBEntities())
            {
                return context.Auction.FirstOrDefault(i => i.Id.Equals(key));
            }
        }

        public void SubscribeCustomer(string id, string customerId)
        {
            if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(customerId))
            {
                throw new BidException(Messages.InvalidInputValues);
            }
            using (var context = new BidDBEntities())
            {
                var customer = context.Customer.Find(customerId);
                if (customer == null)
                {
                    throw new BidException(Messages.InvalidCustomer);
                }
                var auction = context.Auction.Find(id);
                if (auction == null)
                {
                    throw new BidException(Messages.InvalidAuction);
                }
                auction.CustomerAuction.Add(new CustomerAuction()
                {
                    Id = CommonId.NewId,
                    AuctionId = auction.Id,
                    CustomerId = customerId
                });
                context.Auction.Attach(auction);
                context.SaveChanges();
            }
        }
    }
}
